load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

RULES_JVM_EXTERNAL_TAG = "4.0"
RULES_JVM_EXTERNAL_SHA = "31701ad93dbfe544d597dbe62c9a1fdd76d81d8a9150c2bf1ecf928ecdf97169"

http_archive(
    name = "rules_jvm_external",
    sha256 = RULES_JVM_EXTERNAL_SHA,
    strip_prefix = "rules_jvm_external-%s" % RULES_JVM_EXTERNAL_TAG,
    url = "https://github.com/bazelbuild/rules_jvm_external/archive/%s.zip" % RULES_JVM_EXTERNAL_TAG,
)

load("@rules_jvm_external//:defs.bzl", "maven_install")


IO_GRPC_GRPC_JAVA_TAG = "1.35.1"
http_archive(
     name = "io_grpc_grpc_java",
     sha256 = "bcddd1208ba4d7201b9abf0294494d0f5311f01c44f37b8b812a28afc8be9a9c",
     strip_prefix = "grpc-java-%s" % IO_GRPC_GRPC_JAVA_TAG,
     url = "https://github.com/grpc/grpc-java/archive/v%s.zip" % IO_GRPC_GRPC_JAVA_TAG,
)

load("@io_grpc_grpc_java//:repositories.bzl", "IO_GRPC_GRPC_JAVA_ARTIFACTS")
load("@io_grpc_grpc_java//:repositories.bzl", "IO_GRPC_GRPC_JAVA_OVERRIDE_TARGETS")

VERTX_VERSION = "4.0.3"
maven_install(
    artifacts = [
        "org.assertj:assertj-core:3.8.0",
        "io.vertx:vertx-core:%s" % VERTX_VERSION,
        "io.vertx:vertx-web:%s" % VERTX_VERSION,
        "io.vertx:vertx-grpc:%s" % VERTX_VERSION,
        "org.slf4j:slf4j-api:1.7.25",
        "ch.qos.logback:logback-core:1.2.3",
        "ch.qos.logback:logback-classic:1.2.3",
        "org.fusesource.jansi:jansi:2.0",
        "io.vertx:vertx-junit5:%s" % VERTX_VERSION,
        "org.junit.platform:junit-platform-console:jar:1.7.0",
        "org.junit.jupiter:junit-jupiter-api:5.7.0",
        "org.junit.jupiter:junit-jupiter-engine:5.7.0",
    ] + IO_GRPC_GRPC_JAVA_ARTIFACTS,
    repositories = [
        # Private repositories are supported through HTTP Basic auth
        "https://maven.google.com",
        "https://repo1.maven.org/maven2",
    ],
    generate_compat_repositories = True,
    override_targets = IO_GRPC_GRPC_JAVA_OVERRIDE_TARGETS,
    fetch_sources = True,
)

load("@maven//:compat.bzl", "compat_repositories")
compat_repositories()

load("@io_grpc_grpc_java//:repositories.bzl", "grpc_java_repositories")
grpc_java_repositories()

load("@com_google_protobuf//:protobuf_deps.bzl", "protobuf_deps")
protobuf_deps()
