package org.szhao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.jupiter.api.Test;

public class MainVerticleTest {
    private static final Logger logger = LoggerFactory.getLogger(MainVerticleTest.class);

    @Test
    public void test() {
        logger.info("test OK");
    }
}
