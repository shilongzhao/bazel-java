package org.szhao;

import io.vertx.core.Launcher;

public class AppLauncher extends Launcher {
    public static void main(String[] args) {
        new AppLauncher().dispatch(args);
    }

    @Override
    public String getMainVerticle() {
        return MainVerticle.class.getName();
    }
}
